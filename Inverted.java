import java.util.HashMap;

public class Inverted {
    String docId;
    int fileLength;
    String docName;
    int frequency;

    public static void main(String[] args) {
        System.out.println("Hi im main");   
    }

    public Inverted(String docId, int fileLength, String docName) {
        this.docId = docId;
        this.fileLength = fileLength;
        this.docName = docName;
        this.frequency = -1;
    }

    public Inverted(String docId, int fileLength, String docName, int frequency) {
        this.docId = docId;
        this.fileLength = fileLength;
        this.docName = docName;
        this.frequency = frequency;  
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public HashMap<String, Object> returnObject() {
        HashMap<String, Object> obj = new HashMap<>();
        obj.put("docId", this.docId);
        obj.put("fileLength", this.fileLength); 
        obj.put("docName", this.docName);
        obj.put("frequency", this.frequency);

        return obj;
    }
}
