import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.lang.Character;
import java.lang.StringBuilder;

public class ReadFile {
    public static void main(String[] args) {
        try {
            File post = new File("post1.txt");
            File file = new File("file.txt");
            Scanner postReader = new Scanner(post);
            Scanner fileReader = new Scanner(file);

            List<Inverted> list = new ArrayList<Inverted>();
            for (int i=0; i<10; i++) {
                String[] fileDate = fileReader.nextLine().split(" ");
                // list.add(fileDate);
                String docId = fileDate[0];
                int fileLength = Integer.parseInt(fileDate[1]);
                String docName = fileDate[3];

                Inverted invertedFile = new Inverted(
                    docId, fileLength, docName 
                );
                // list.add(invertedFile); 
                System.out.println(invertedFile.returnObject());
            }
            // System.out.println(list);
            fileReader.close();
            postReader.close();

            System.out.println("-------------new line here--------------\n");

            File queryT = new File("queryT");
            File queryTDN = new File("queryTDN");
            Scanner queryTReader = new Scanner(queryT);
            Scanner queryTDNReader = new Scanner(queryTDN);

            Map<String, String[]> mapT = new HashMap<String, String[]>();
            Map<String, String[]> mapTDN = new HashMap<String, String[]>();
            for (int i=0; i<10; i++) {
                String[] arrT = queryTReader.nextLine().split(" ");
                String[] arrTDN = queryTDNReader.nextLine().split(" ");

                String queryTId = arrT[0];
                String[] queryTValue = Arrays.copyOfRange(arrT, 1, arrT.length);
                String queryTDNId = arrTDN[0];
                String[] queryTDNValue = Arrays.copyOfRange(arrTDN, 1, arrTDN.length);

                for (int k = 0; k < queryTValue.length; k++) {
                    String word = queryTValue[k];
                    // System.out.println("now working on: " + word);
                    int len = word.length();
                    char[] charArr = new char[len];

                    for (int j = 0; j < len; j ++) {
                        charArr[j] = word.charAt(j);
                    }

                    Stemmer stemmer = new Stemmer();
                    stemmer.add(charArr, charArr.length);
                    stemmer.stem();
                    StringBuilder result = new StringBuilder(stemmer.toString());
                    result.setCharAt(result.length() - 1, Character.toUpperCase(result.charAt(result.length() - 1)));         
                    // System.out.println("stemmed: " + result);           
                    queryTValue[k] = result.toString();
                }

                System.out.println("queryTId: " + queryTId + " , queryTValue: " + Arrays.toString(queryTValue));
                mapT.put(queryTId, queryTValue);
                // System.out.println("T: " + Arrays.toString(arrT));
                // System.out.println("TDN: " + Arrays.toString(arrTDN));
            }  
            // mapT.entrySet().forEach(entry -> {
            //     System.out.println(entry.getKey() + ": " + Arrays.toString(entry.getValue()));
            // });

            for (String key : mapT.keySet()) {
                System.out.println(key + ": " + Arrays.toString(mapT.get(key)));
            }

            queryTReader.close();
            queryTDNReader.close();
            
        } catch (FileNotFoundException e){
            System.out.println(e);
            e.printStackTrace();
        }
    }
}
