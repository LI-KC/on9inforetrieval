import java.util.Map;
import java.util.stream.IntStream;
import java.util.HashMap;
import java.util.Iterator;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class DocumentLength {
    private static Map<String, HashNode> map = new HashMap<>();
    private static int numberOfDocuments = 64814;
    private static double documentLength[] = new double[numberOfDocuments];
    
    public static void main(String[] args) {
        String fileName = "inverted.txt";
        String line;
        String stem;
        int documentFrequency;
        int documentID;
        int frequency;
        
        try {
            FileReader fileReader = new FileReader(fileName);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            
            while((line = bufferedReader.readLine()) != null) {
                String[] data = line.split(":");
                String[] documentData = data[0].split(" ");
                stem = documentData[0];
                documentFrequency = Integer.parseInt(documentData[1]);
                HashNode hashNode = new HashNode();
                
                String[] postingsData = data[1].split(",");
                for(int i = 0; i < documentFrequency; i++) {
                    String[] postingData = postingsData[i].split(" ");
                    documentID = Integer.parseInt(postingData[0]);
                    frequency = Integer.parseInt(postingData[1]);
                    
                    hashNode.loadPosting(documentID, frequency, numberOfDocuments);
                }
                map.put(stem, hashNode);
            }
            bufferedReader.close();
        }
        catch(FileNotFoundException e) {}
        catch(IOException e) {}
        
        try {
            calculate();
            save();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    private static void calculate() {
        IntStream.range(0,numberOfDocuments).parallel().forEach(i->{
            Iterator<HashNode> iterator = map.values().iterator();
            while (iterator.hasNext()) {
                HashNode node = iterator.next();
                documentLength[i] += Math.pow(node.getInverseDocumentFrequency() * node.getTermFrequency(i), 2);
            }
            documentLength[i] = Math.sqrt(documentLength[i]);
        });
    }
    
    private static void save() throws IOException {
        try (FileWriter fileWriter = new FileWriter("doclen.txt");
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {
            for (int i = 0; i < documentLength.length; i++) {
                StringBuilder line = new StringBuilder(Integer.toString(i));
                line.append(" " + documentLength[i]);

                bufferedWriter.write(line.toString() + "\n");
            }
       
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
