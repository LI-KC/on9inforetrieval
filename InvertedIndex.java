import java.util.Map;
import java.util.HashMap;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class InvertedIndex {
    private static Map<String, HashNode> map = new HashMap<>();
    
    public static void main(String[] args) {
        String fileName = "post1.txt";
        String line;
        String stem;
        int documentID;
        
        try {
            FileReader fileReader = new FileReader(fileName);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            
            while((line = bufferedReader.readLine()) != null) {
                String[] data = line.split(" ");
                stem = data[0];
                documentID = Integer.parseInt(data[1]);
                
                add(stem, documentID);
            }
            bufferedReader.close();
        }
        catch(FileNotFoundException e) {}
        catch(IOException e) {}
        
        try {
            save();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    private static void add(String stem, int documentID) {
        if(map.containsKey(stem)) { // if stem was already added
            HashNode node = map.get(stem);
            node.addPosting(documentID);
        }
        else { // if stem was not added
            HashNode node = new HashNode();
            node.addPosting(documentID);
            
            map.put(stem, node);
        }
    }
    
    private static void save() throws IOException {
        try (FileWriter fileWriter = new FileWriter("inverted.txt");
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {
            for (Map.Entry<String, HashNode> term_node : map.entrySet()) {
                String term = term_node.getKey();
                HashNode node = term_node.getValue();
                
                StringBuilder line = new StringBuilder(term);
                line.append(" " + node.getDocumentFrequency() + ":");
                
                int[] documentIDs = node.getPostings();
                int documentID, frequency;
                for (int i = 0; i < documentIDs.length; i++) {
                    documentID = documentIDs[i];
                    frequency = node.getTermFrequency(documentID);
                    line.append(documentID + " " + frequency + ",");
                }
                bufferedWriter.write(line.toString() + "\n");
            }
       
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
