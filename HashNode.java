import java.util.Map;
import java.util.Arrays;
import java.util.HashMap;

public class HashNode {
    private Map<Integer, Integer> postings;
    private double inverseDocumentFrequency;
    
    public HashNode() {
        postings = new HashMap<>();
    }
    
    public int getTermFrequency(int documentID) {
        if(postings.containsKey(documentID)) {
            return postings.get(documentID);
        }
        else {
            return 0;
        }
    }
    
    public void loadPosting(int documentID, int frequency, int numberOfDocuments) {
        postings.put(documentID, frequency);
        inverseDocumentFrequency = Math.log10(numberOfDocuments/getDocumentFrequency());
    }
    
    public void newPosting(int documentID) {
        postings.put(documentID, 1);
    }
    
    public void incrasePosting(int documentID) {
        postings.put(documentID, postings.get(documentID) + 1);
    }
    
    public void addPosting(int documentID) {
        if(postings.containsKey(documentID)) {
            incrasePosting(documentID);
        }
        else {
            newPosting(documentID);
        }
    }
    
    public int getDocumentFrequency() {
        return postings.size();
    }
    
    public double getInverseDocumentFrequency() {
        return inverseDocumentFrequency;
    }
    
    public int[] getPostings(){
        int[] documentIDs = postings.keySet().stream().mapToInt(Number::intValue).toArray();
        Arrays.sort(documentIDs);
        return documentIDs;
    }
}
